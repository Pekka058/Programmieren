package Gui;

import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

public class Gui {
	public void create() {
		ImageIcon img = new ImageIcon("img/icon.png");
		
		JFrame jf = new JFrame("MOTeam-Programm v0.01");
		int width = 1280;
		int height = 720;
		jf.setSize(width, height);
		jf.setResizable(false);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.setIconImage(img.getImage());
		
		
		jf.setVisible(true);
	}
}
