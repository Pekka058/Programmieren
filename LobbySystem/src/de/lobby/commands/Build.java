package de.lobby.commands;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import de.lobby.inventorys.startInventory;
import de.lobby.main.Main;
@SuppressWarnings("static-access")
public class Build implements CommandExecutor, Listener {

	public static ArrayList<Player> build = new ArrayList<>();
	public static HashMap<Player, ItemStack[]> invSave = new HashMap<>();

	File lobby = new File("plugins/LobbySystem", "LobbySystem.yml");
	FileConfiguration lobbyfc = YamlConfiguration.loadConfiguration(lobby);

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player p = (Player) sender;
		if (p.hasPermission("Lobby.Build")) {
			if (args.length == 0) {
				if (build.contains(p)) {
					build.remove(p);
					p.setGameMode(GameMode.SURVIVAL);
					startInventory.setStartinv(p);
					p.sendMessage(Main.getInstance().TestPrefix + "�cBuild modus aus!");
					p.getInventory().setContents(invSave.get(p));
					invSave.remove(p);

				} else {
					build.add(p);
					invSave.put(p, p.getInventory().getContents());
					p.getInventory().clear();
					p.setGameMode(GameMode.CREATIVE);
					p.sendMessage(Main.getInstance().TestPrefix + "�aBuild modus an!");

				}

			} else
				p.sendMessage(Main.getInstance().prefix + Main.getInstance().Rechte);

		}

		return true;
	}

	@EventHandler
	public void onBreak(BlockBreakEvent e) {

	}

}
