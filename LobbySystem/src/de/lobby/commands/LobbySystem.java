package de.lobby.commands;

import java.io.File;
import java.io.IOException;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import de.lobby.inventorys.startInventory;
import de.lobby.main.Main;

public class LobbySystem implements CommandExecutor {

	File lobby = new File("plugins/LobbySystem", "config.yml");
	FileConfiguration lobbyfc = YamlConfiguration.loadConfiguration(lobby);

	@SuppressWarnings("static-access")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (sender instanceof Player) {
			Player p = (Player) sender;
			if (p.hasPermission("ls.ls")) {
				if (args.length == 0) {

					p.sendMessage(Main.getInstance().TestPrefix + "§8 ▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀");
					p.sendMessage(Main.getInstance().TestPrefix + "§cUsage: /ls setgame <Game mode>     §8█");
					p.sendMessage(Main.getInstance().TestPrefix + "§cUsage: /ls Spawn                   §8█");
					p.sendMessage(Main.getInstance().TestPrefix + "§8 ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄");
				}
				if (args.length == 1) {

					if (args[0].equalsIgnoreCase("setgame")) {
						p.sendMessage(Main.getInstance().TestPrefix + "§cBitte gebe einen Spiel modus an!");
						p.sendMessage(Main.getInstance().TestPrefix
								+ "§7§lKnockIT, SkyWars, BedWars, Comunity, MLGRush, FFA, GunGame");

					} else if (args[0].equalsIgnoreCase("setspawn")) {

						Location setSpawn = p.getLocation();
						lobbyfc.set("Lobby.lobby.pos.x", setSpawn.getBlockX());
						lobbyfc.set("Lobby.lobby.pos.y", setSpawn.getBlockY());
						lobbyfc.set("Lobby.lobby.pos.z", setSpawn.getBlockZ());
						lobbyfc.set("Lobby.lobby.pos.yaw", setSpawn.getYaw());
						lobbyfc.set("Lobby.lobby.pos.pitch", setSpawn.getPitch());
						lobbyfc.set("Lobby.lobby.pos.world", setSpawn.getWorld().getName());
						p.sendMessage(Main.getInstance().TestPrefix + "§6Die Lobby wurde erfolgreich gesetzt");
						startInventory.setStartinv(p);
						try {
							lobbyfc.save(lobby);
						} catch (IOException e) {
							e.printStackTrace();
						}

					}

				}
				if (args.length == 2) {

					if (args[1].equalsIgnoreCase("Bedwars")) {
						Location bedwars = p.getLocation();
						lobbyfc.set("Lobby.bedwars.pos.x", bedwars.getBlockX());
						lobbyfc.set("Lobby.bedwars.pos.y", bedwars.getBlockY());
						lobbyfc.set("Lobby.bedwars.pos.z", bedwars.getBlockZ());
						lobbyfc.set("Lobby.bedwars.pos.yaw", bedwars.getYaw());
						lobbyfc.set("Lobby.bedwars.pos.pitch", bedwars.getPitch());
						lobbyfc.set("Lobby.bedwars.pos.world", bedwars.getWorld().getName());
						p.sendMessage(Main.getInstance().TestPrefix + "§aDu hast den Spawn für §6BedWars §agesetzt!");
						try {
							lobbyfc.save(lobby);
						} catch (IOException e) {
							e.printStackTrace();
						}

					}
					if (args[1].equalsIgnoreCase("FFA")) {
						Location ffa = p.getLocation();
						lobbyfc.set("Lobby.ffa.pos.x", ffa.getBlockX());
						lobbyfc.set("Lobby.ffa.pos.y", ffa.getBlockY());
						lobbyfc.set("Lobby.ffa.pos.z", ffa.getBlockZ());
						lobbyfc.set("Lobby.ffa.pos.yaw", ffa.getYaw());
						lobbyfc.set("Lobby.ffa.pos.pitch", ffa.getPitch());
						lobbyfc.set("Lobby.ffa.pos.world", ffa.getWorld().getName());
						p.sendMessage(Main.getInstance().TestPrefix + "§aDu hast den Spawn für §6FFA §agesetzt!");
						try {
							lobbyfc.save(lobby);
						} catch (IOException e) {
							e.printStackTrace();
						}

					}
					if (args[1].equalsIgnoreCase("gungame")) {
						Location skywars = p.getLocation();
						lobbyfc.set("Lobby.gungame.pos.x", skywars.getBlockX());
						lobbyfc.set("Lobby.gungame.pos.y", skywars.getBlockY());
						lobbyfc.set("Lobby.gungame.pos.z", skywars.getBlockZ());
						lobbyfc.set("Lobby.gungame.pos.yaw", skywars.getYaw());
						lobbyfc.set("Lobby.gungame.pos.pitch", skywars.getPitch());
						lobbyfc.set("Lobby.gungame.pos.world", skywars.getWorld().getName());
						p.sendMessage(Main.getInstance().TestPrefix + "§aDu hast den Spawn für §6GunGame §agesetzt!");
						try {
							lobbyfc.save(lobby);
						} catch (IOException e) {
							e.printStackTrace();
						}

					}
					if (args[1].equalsIgnoreCase("Knockit")) {
						Location Knockit = p.getLocation();
						lobbyfc.set("Lobby.Knockit.pos.x", Knockit.getBlockX());
						lobbyfc.set("Lobby.Knockit.pos.y", Knockit.getBlockY());
						lobbyfc.set("Lobby.Knockit.pos.z", Knockit.getBlockZ());
						lobbyfc.set("Lobby.Knockit.pos.yaw", Knockit.getYaw());
						lobbyfc.set("Lobby.Knockit.pos.pitch", Knockit.getPitch());
						lobbyfc.set("Lobby.Knockit.pos.world", Knockit.getWorld().getName());
						p.sendMessage(Main.getInstance().TestPrefix + "§aDu hast den Spawn für §6KnockIT §agesetzt!");
						try {
							lobbyfc.save(lobby);
						} catch (IOException e) {
							e.printStackTrace();
						}

					}
					if (args[1].equalsIgnoreCase("SkyWars")) {
						Location SkyWars = p.getLocation();
						lobbyfc.set("Lobby.skywars.pos.x", SkyWars.getBlockX());
						lobbyfc.set("Lobby.skywars.pos.y", SkyWars.getBlockY());
						lobbyfc.set("Lobby.skywars.pos.z", SkyWars.getBlockZ());
						lobbyfc.set("Lobby.skywars.pos.yaw", SkyWars.getYaw());
						lobbyfc.set("Lobby.skywars.pos.pitch", SkyWars.getPitch());
						lobbyfc.set("Lobby.skywars.pos.world", SkyWars.getWorld().getName());
						p.sendMessage(Main.getInstance().TestPrefix + "§aDu hast den Spawn für §6SkyWars §agesetzt!");
						try {
							lobbyfc.save(lobby);
						} catch (IOException e) {
							e.printStackTrace();
						}

					}
					if (args[1].equalsIgnoreCase("Comunity")) {
						Location Comunity = p.getLocation();
						lobbyfc.set("Lobby.Comunity.pos.x", Comunity.getBlockX());
						lobbyfc.set("Lobby.Comunity.pos.y", Comunity.getBlockY());
						lobbyfc.set("Lobby.Comunity.pos.z", Comunity.getBlockZ());
						lobbyfc.set("Lobby.Comunity.pos.yaw", Comunity.getYaw());
						lobbyfc.set("Lobby.Comunity.pos.pitch", Comunity.getPitch());
						lobbyfc.set("Lobby.Comunity.pos.world", Comunity.getWorld().getName());
						p.sendMessage(
								Main.getInstance().TestPrefix + "§aDu hast den Spawn für die §6Comunity §agesetzt!");
						try {
							lobbyfc.save(lobby);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					if (args[1].equalsIgnoreCase("mlgrush")) {
						Location mlg = p.getLocation();

						lobbyfc.set("Lobby.mlg.pos.x", mlg.getBlockX());
						lobbyfc.set("Lobby.mlg.pos.y", mlg.getBlockY());
						lobbyfc.set("Lobby.mlg.pos.z", mlg.getBlockZ());
						lobbyfc.set("Lobby.mlg.pos.yaw", mlg.getYaw());
						lobbyfc.set("Lobby.mlg.pos.pitch", mlg.getPitch());
						lobbyfc.set("Lobby.mlg.pos.world", mlg.getWorld().getName());
						p.sendMessage(
								Main.getInstance().TestPrefix + "§aDu hast den Spawn für die §6MLG-Rush §agesetzt!");
						try {
							lobbyfc.save(lobby);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}

				}

			} else
				p.sendMessage(Main.getInstance().prefix + Main.getInstance().Rechte);

		}

		return true;
	}
}
