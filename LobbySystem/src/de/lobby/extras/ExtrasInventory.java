package de.lobby.extras;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import de.lobby.itemBuilder.ItemBuilder;

public class ExtrasInventory implements Listener {

	@EventHandler
	public void onInterExtras(PlayerInteractEvent e) {
		Player player = e.getPlayer();
		if (e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
			if (player.getItemInHand().getType() == Material.CHEST) {

				Inventory inventory = Bukkit.createInventory(null, 9 * 1, "�6Extras");

				ItemStack boots = new ItemBuilder(Material.GOLD_BOOTS).setDisplayName("�6Boots").setLore("�cKlick me").build();
				ItemStack fly = new ItemBuilder(Material.FEATHER).setDisplayName("�5Fly").build();
				ItemStack remove = new ItemBuilder(Material.BARRIER).setDisplayName("�cweg").build();
				

				inventory.setItem(4, boots);
				inventory.setItem(5, fly);
				inventory.setItem(0, remove);
				
				player.getInventory().setBoots(boots);
				player.getInventory().setItem(1, fly);

				player.openInventory(inventory);
			}
		}

	}
	
	@SuppressWarnings("unlikely-arg-type")
	@EventHandler
	public void onExtrasClick(InventoryClickEvent e) {
		Player player = (Player) e.getWhoClicked();
		if(e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
			if(e.getInventory().getName().equalsIgnoreCase("�6Extras")) {
				e.setCancelled(true);
				if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�cweg")) {
					player.closeInventory();
					player.getInventory().remove(Material.GOLD_BOOTS);

				}
			}
		}
		
	}

}
