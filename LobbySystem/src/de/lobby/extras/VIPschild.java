package de.lobby.extras;

import java.util.HashMap;

import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import de.lobby.commands.Build;
import de.lobby.main.Main;

public class VIPschild implements Listener {

	private String schild = "�8[�5VIP-Schild�8] ";

	private Main plugin;

	public VIPschild(Main main) {
		this.plugin = main;
	}

	HashMap<Player, BukkitRunnable> run = new HashMap<>();

	@EventHandler
	public void onInteractPlayer(PlayerInteractEvent e) {
		final Player p = e.getPlayer();
		if (Build.build.contains(p)) {
			e.setCancelled(false);
		} else if (!Build.build.contains(p)) {
			e.setCancelled(true);
			if (e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
				if (p.getItemInHand().getType() == Material.EYE_OF_ENDER) {
					e.setCancelled(true);
					if (run.containsKey(p)) {
						p.sendMessage(schild + "�cSchutz-schild wurde ausgeschalten!");
						p.playSound(p.getLocation(), Sound.LEVEL_UP, 4, 3);
						run.get(p).cancel();
						run.remove(p);
					} else if (!run.containsKey(p)) {
						run.put(p, new BukkitRunnable() {

							@Override
							public void run() {
								p.getWorld().playEffect(p.getLocation(), Effect.ENDER_SIGNAL, 3);

							}
						});
						run.get(p).runTaskTimer((Plugin) plugin, 20, 20);
						p.sendMessage(schild + "�aSchutz-schild wurde angeschalten!");
						p.playSound(p.getLocation(), Sound.LEVEL_UP, 4, 3);
					}
				}

			}
		}
	}

	@EventHandler
	public void onQuidt(PlayerQuitEvent e) {
		if (run.containsKey(e.getPlayer())) {
			run.get(e.getPlayer()).cancel();
			run.remove(e.getPlayer());
		}
	}

	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		Player p = e.getPlayer();

		for (Player players : run.keySet()) {
			if (p != players) {
				if (p.getLocation().distance(players.getLocation()) <= 5) {

					double Ax = p.getLocation().getX();
					double Ay = p.getLocation().getY();
					double Az = p.getLocation().getZ();

					double Bx = players.getLocation().getX();
					double By = players.getLocation().getY();
					double Bz = players.getLocation().getZ();

					double x = Ax - Bx;
					double y = Ay - By;
					double z = Az - Bz;
					org.bukkit.util.Vector v = new org.bukkit.util.Vector(x, y, z).normalize().multiply(2D).setY(0.3D);
					p.setVelocity(v);

				}
			}
		}

		if (run.containsKey(p)) {

			for (Entity entity : p.getNearbyEntities(5, 5, 5)) {
				if (entity instanceof Player) {
					Player target = (Player) entity;
					if (p != target) {

						double Ax = p.getLocation().getX();
						double Ay = p.getLocation().getY();
						double Az = p.getLocation().getZ();

						double Bx = target.getLocation().getX();
						double By = target.getLocation().getY();
						double Bz = target.getLocation().getZ();

						double x = Ax - Bx;
						double y = Ay - By;
						double z = Az - Bz;
						org.bukkit.util.Vector v = new org.bukkit.util.Vector(x, y, z).normalize().multiply(2D)
								.setY(0.3D);
						target.setVelocity(v);

					}

				}
			}

		}

	}

}
