package de.lobby.warps;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import de.lobby.main.Main;

public class warp_Command implements CommandExecutor {
	
	File lobby = new File("plugins/LobbySystem", "Warps.yml");
	FileConfiguration lobbyfc = YamlConfiguration.loadConfiguration(lobby);
	
	@SuppressWarnings("static-access")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(sender instanceof Player) {
			Player p = (Player)sender;
			if(p.hasPermission("lb.warp")) {
				if(args.length == 0) {
					
					p.sendMessage(Main.prefix + "�cUsage: /Warp <1/2/3/4/5>");
				}
				if(args.length == 1) {
					
					if(args[0].equalsIgnoreCase("1")) {
						
						double x = lobbyfc.getDouble("Warps.warp1.x");
						double y = lobbyfc.getDouble("Warps.warp1.y");
						double z = lobbyfc.getDouble("Warps.warp1.z");
						float yaw = (float) lobbyfc.getDouble("Warps.warp1.yaw");
						float pitch = (float) lobbyfc.getDouble("Warps.warp1.pitch");
						World world = Bukkit.getWorld(lobbyfc.getString("Warps.warp1.world"));
						Location Warp1 = new Location(world, x, y, z, yaw, pitch);
						p.teleport(Warp1);
						
						
					}
					if(args[0].equalsIgnoreCase("2")) {
						
						double x = lobbyfc.getDouble("Warps.warp2.x");
						double y = lobbyfc.getDouble("Warps.warp2.y");
						double z = lobbyfc.getDouble("Warps.warp2.z");
						float yaw = (float) lobbyfc.getDouble("Warps.warp2.yaw");
						float pitch = (float) lobbyfc.getDouble("Warps.warp2.pitch");
						World world = Bukkit.getWorld(lobbyfc.getString("Warps.warp2.world"));
						Location Warp2 = new Location(world, x, y, z, yaw, pitch);
						p.teleport(Warp2);
					}
				}
				
				
				
				
			} else
				p.sendMessage(Main.getInstance().prefix + Main.getInstance().Rechte);
			
		}
		
		return true;
	}

}
