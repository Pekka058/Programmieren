package de.lobby.warps;

import java.io.File;
import java.io.IOException;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import de.lobby.main.Main;

public class setWarps implements CommandExecutor {

	File lobby = new File("plugins/LobbySystem", "Warps.yml");
	FileConfiguration lobbyfc = YamlConfiguration.loadConfiguration(lobby);

	@SuppressWarnings("static-access")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (sender instanceof Player) {
			Player p = (Player) sender;
			if (p.hasPermission("lb.setwarp")) {
				if (args.length == 0) {

					p.sendMessage("�cUsage: /setWarp <1/2/3/4/5>");

				}
				if (args.length == 1) {

					if (args[0].equalsIgnoreCase("1")) {
						if (lobbyfc.getString("Warps.warp1.x") != null) {
							Location Warp1 = p.getLocation();

							lobbyfc.set("Warps.warp1.x", Warp1.getBlockX());
							lobbyfc.set("Warps.warp1.y", Warp1.getBlockY());
							lobbyfc.set("Warps.warp1.z", Warp1.getBlockZ());
							lobbyfc.set("Warps.warp1.yaw", Warp1.getYaw());
							lobbyfc.set("Warps.warp1.pitch", Warp1.getPitch());
							lobbyfc.set("Warps.warp1.world", Warp1.getWorld().getName());
							try {
								lobbyfc.save(lobby);
							} catch (IOException e) {
								e.printStackTrace();
							}
						} else if (p.hasPermission("Warp.see")) {
							p.sendMessage(Main.getInstance().TestPrefix + "�cDer Warp-punk �61 �cwurde noch nicht gesetzt!");

						} else {
							p.sendMessage(Main.getInstance().TestPrefix + "�cDer Warp-punkt exestiert nicht!");
						}
					}
					if (args[0].equalsIgnoreCase("2")) {
						if (lobbyfc.getString("Warps.warp2.x") != null) {
							Location Warp2 = p.getLocation();

							lobbyfc.set("Warps.warp2.x", Warp2.getBlockX());
							lobbyfc.set("Warps.warp2.y", Warp2.getBlockY());
							lobbyfc.set("Warps.warp2.z", Warp2.getBlockZ());
							lobbyfc.set("Warps.warp2.yaw", Warp2.getYaw());
							lobbyfc.set("Warps.warp2.pitch", Warp2.getPitch());
							lobbyfc.set("Warps.warp2.world", Warp2.getWorld().getName());
							try {
								lobbyfc.save(lobby);
							} catch (IOException e) {
								e.printStackTrace();
							}
						} else if(p.hasPermission("Warp.see")) {
							p.sendMessage(Main.getInstance().TestPrefix + "�cDer Warp-punk �62 �cwurde noch nicht gesetzt!");
							
						} else {
							p.sendMessage(Main.getInstance().TestPrefix + "�cDer Warp-punkt exestiert nicht!");
						}
					}
				}

			} else
				p.sendMessage(Main.getInstance().prefix + Main.getInstance().Rechte);
		}

		return true;
	}

}
