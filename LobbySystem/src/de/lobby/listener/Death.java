package de.lobby.listener;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import de.lobby.commands.Build;
import de.lobby.inventorys.startInventory;
import de.lobby.main.Main;

public class Death implements Listener {
	
	File lobby = new File("plugins/LobbySystem", "config.yml");
	FileConfiguration lobbyfc = YamlConfiguration.loadConfiguration(lobby);
	
	@EventHandler
	public void onDeath(PlayerDeathEvent e) {
		Player p = e.getEntity().getPlayer();
		startInventory.setStartinv(p);
		
		e.setDeathMessage(Main.getInstance().TestPrefix + "");
	}
	@EventHandler
	public void onRespawn(PlayerRespawnEvent e) {
		Player player = e.getPlayer();
		double x = lobbyfc.getDouble("Lobby.lobby.pos.x");
		double y = lobbyfc.getDouble("Lobby.lobby.pos.y");
		double z = lobbyfc.getDouble("Lobby.lobby.pos.z");
		float yaw = (float) lobbyfc.getDouble("Lobby.lobby.pos.yaw");
		float pitch = (float) lobbyfc.getDouble("Lobby.lobby.pos.pitch");
		World world = Bukkit.getWorld(lobbyfc.getString("Lobby.lobby.pos.world"));
		Location lobby = new Location(world, x, y, z, yaw, pitch);
		e.setRespawnLocation(lobby);
		Build.invSave.get(player);
	}
	
	@EventHandler
	public void onMob(EntityDamageByEntityEvent e) {
		e.setCancelled(true);
	}
}
