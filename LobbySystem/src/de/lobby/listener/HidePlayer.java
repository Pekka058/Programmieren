package de.lobby.listener;

import java.util.concurrent.CopyOnWriteArrayList;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import de.lobby.itemBuilder.ItemBuilder;

public class HidePlayer implements Listener {

	// private static ArrayList<Player> hidden = new ArrayList<>();
	public static CopyOnWriteArrayList<Player> hidden = new CopyOnWriteArrayList<>();

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		if (e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
			Player p = e.getPlayer();
			if (p.getItemInHand().getType() == Material.BLAZE_ROD) {
				if (p.getItemInHand() != null) {

					Inventory inv = Bukkit.createInventory(null, 9 * 1, "�cPlayer Hidder");

					ItemStack showall = new ItemBuilder(Material.WOOL, (short) 13).setDisplayName("�aalle").build();
					ItemStack showvip = new ItemBuilder(Material.WOOL, (short) 10).setDisplayName("�5VIP").build();
					ItemStack showkeinen = new ItemBuilder(Material.WOOL, (short) 14).setDisplayName("�cKeinen").build();

					for (int i = 0; i < 9 * 1; i++) {
						ItemStack leer = new ItemBuilder(Material.STAINED_GLASS_PANE, (short) 15).setDisplayName(" ")
								.build();

						inv.setItem(i, leer);
					}

					inv.setItem(2, showall);
					inv.setItem(4, showvip);
					inv.setItem(6, showkeinen);

					p.openInventory(inv);
				}
			}
		}
	}

	@SuppressWarnings("unlikely-arg-type")
	@EventHandler
	public void handleClick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		if (e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
			if (e.getCurrentItem().getType().equals(new ItemBuilder(Material.WOOL, (short) 13))) {
				if(e.getInventory().getTitle().equalsIgnoreCase("�cPlayer Hider")) {
					
					for(Player all : Bukkit.getOnlinePlayers()) {
						
						p.hidePlayer(all);
					}
					p.closeInventory();
				}
				
				

			}

		}
	}

}