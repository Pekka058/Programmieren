package de.lobby.listener;

import java.util.HashMap;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import de.lobby.commands.Build;
import de.lobby.itemBuilder.ItemBuilder;
import de.lobby.main.Main;

public class Teleporter implements Listener {

	@SuppressWarnings("unused")
	private HashMap<Player, Integer> continueum = new HashMap<>();
	public Main plugin;

	public Teleporter(Main instance) {
		plugin = instance;
	}

	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		if (e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
			if (e.getItem() != null) {
				Player p = e.getPlayer();
				if (p.getItemInHand().getType() == Material.COMPASS) {
					if (e.getItem().getType().equals(Material.COMPASS)) {
						if (Build.build.contains(p)) {
							e.setCancelled(false);
						} else if (!Build.build.contains(p)) {
							e.setCancelled(true);

							Inventory inv = Bukkit.createInventory(null, 9 * 3, "�7Navigator");

							ItemStack spawn = new ItemBuilder(Material.NETHER_STAR).setDisplayName("�6Spawn").setLore("Kick zum Teleportieren").build();
							ItemStack Knockit = new ItemBuilder(Material.STICK).setDisplayName("�6Knockit").setLore("Kick zum Teleportieren").build();
							ItemStack BedWars = new ItemBuilder(Material.BED).setDisplayName("�9Bed�bWars").setLore("Kick zum Teleportieren").build();
							ItemStack GunGame = new ItemBuilder(Material.WOOD_AXE).setDisplayName("�5GunGame").setLore("Kick zum Teleportieren").build();
							ItemStack SkyWars = new ItemBuilder(Material.DIAMOND_ORE).setDisplayName("�bSkyWars").setLore("Kick zum Teleportieren").build();
							ItemStack MLG = new ItemBuilder(Material.BLAZE_ROD).setDisplayName("�6MLG-Rush").setLore("Kick zum Teleportieren").build();
							ItemStack Comunity = new ItemBuilder(Material.SKULL_ITEM).setDisplayName("�6Comunity").setLore("Kick zum Teleportieren").build();
							ItemStack FFA = new ItemBuilder(Material.IRON_SWORD).setDisplayName("�7FFA").setLore("Kick zum Teleportieren").build();

							for (int i = 0; i < 9 * 3; i++) {
								ItemStack s = new ItemBuilder(Material.STAINED_GLASS_PANE).setDisplayName(" ").build();
								inv.setItem(i, s);

							}

							inv.setItem(13, spawn);
							inv.setItem(6, Knockit);
							inv.setItem(2, BedWars);
							inv.setItem(20, GunGame);
							inv.setItem(24, SkyWars);
							inv.setItem(10, MLG);
							inv.setItem(22, Comunity);
							inv.setItem(16, FFA);

							p.openInventory(inv);
						}

					}
				}

			}

		}

	}

}
