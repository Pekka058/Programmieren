package de.lobby.listener;

import java.io.File;
import java.io.IOException;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import de.lobby.main.Main;

public class Prefix {

	public static void readConfig() {
		FileConfiguration cfg = getConfigFileConfiguration();
		Main.getInstance().TestPrefix = ChatColor.translateAlternateColorCodes('&', cfg.getString("prefix")) + " �r";
	}

	public static FileConfiguration getConfigFileConfiguration() {
		return YamlConfiguration.loadConfiguration(getConfigFile());
	}

	public static File getConfigFile() {
		return new File("plugins/LobbySystem", "prefix.yml");
	}

	public static void setStartConfig() {
		FileConfiguration cfg = getConfigFileConfiguration();
		cfg.options().copyDefaults(true);
		cfg.addDefault("prefix", "�8[�6�lLobby�8] ");
		try {
			cfg.save(getConfigFile());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void readKeineRechte() {
		FileConfiguration cfg = getConfigKeineRechte();
		Main.getKeineRechte().Rechte = ChatColor.translateAlternateColorCodes('&', cfg.getString("Rechte")) + " �r";
	}

	public static FileConfiguration getConfigKeineRechte() {
		return YamlConfiguration.loadConfiguration(getKeineRechte());
	}

	public static File getKeineRechte() {
		return new File("plugins/LobbySystem", "prefix.yml");
	}

	public static void setStartRechte() {
		FileConfiguration cfg = getConfigKeineRechte();
		cfg.options().copyDefaults(true);
		cfg.addDefault("Rechte", "&cKeine Rechte!");
		try {
			cfg.save(getKeineRechte());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
