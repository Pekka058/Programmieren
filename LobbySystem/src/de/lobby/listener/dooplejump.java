package de.lobby.listener;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.util.Vector;

public class dooplejump implements Listener {

	HashMap<Player, Boolean> cooldown = new HashMap<Player, Boolean>();
	ArrayList<Player> smash = new ArrayList<Player>();

	File lobby = new File("plugins/LobbySystem", "LobbySystem.yml");
	FileConfiguration lobbyfc = YamlConfiguration.loadConfiguration(lobby);

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onDamage(EntityDamageEvent e) {
		if (e.getCause() == DamageCause.FALL) {
			if (e.getEntity().getType() == EntityType.PLAYER) {
				Player p = (Player) e.getEntity();
				if (p.getWorld().getName().equalsIgnoreCase(lobbyfc.getString("Lobby.world.w"))) {
					if (p.hasPermission("dooplejump.use")) {

						e.setCancelled(true);

						if (smash.contains(p)) {
							ArrayList<Block> blocks = new ArrayList<Block>();
							blocks.add(p.getWorld().getBlockAt(p.getLocation().subtract(0, 1, 0)));
							blocks.add(p.getWorld().getBlockAt(p.getLocation().subtract(1, 1, 0)));
							blocks.add(p.getWorld().getBlockAt(p.getLocation().subtract(0, 1, 1)));
							blocks.add(p.getWorld().getBlockAt(p.getLocation().subtract(-1, 1, 0)));
							blocks.add(p.getWorld().getBlockAt(p.getLocation().subtract(0, 1, -1)));
							blocks.add(p.getWorld().getBlockAt(p.getLocation().subtract(1, 1, 1)));
							blocks.add(p.getWorld().getBlockAt(p.getLocation().subtract(-1, 1, -1)));
							blocks.add(p.getWorld().getBlockAt(p.getLocation().subtract(2, 1, 0)));
							blocks.add(p.getWorld().getBlockAt(p.getLocation().subtract(-2, 1, 0)));
							blocks.add(p.getWorld().getBlockAt(p.getLocation().subtract(0, 1, 2)));
							blocks.add(p.getWorld().getBlockAt(p.getLocation().subtract(0, 1, -2)));
							for (Block b : blocks) {
								for (Player pl : Bukkit.getOnlinePlayers()) {
									pl.playEffect(b.getLocation(), Effect.STEP_SOUND, b.getTypeId());
								}
							}

							smash.remove(p);

						}
					}
				}
			}
		}
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		Player p = e.getPlayer();

		if (p.getGameMode() == GameMode.CREATIVE)
			return;

		if (cooldown.get(p) != null && cooldown.get(p) == true) {
			p.setAllowFlight(true);
		} else {
			p.setAllowFlight(false);
		}

		if (p.isOnGround()) {
			cooldown.put(p, true);
		}

		if (cooldown.get(p) != null && cooldown.get(p) == false) {
			for (Player pl : Bukkit.getOnlinePlayers()) {
				pl.playEffect(p.getLocation(), Effect.SMOKE, 2004);
			}
		}
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onFly(PlayerToggleFlightEvent e) {
		Player p = e.getPlayer();

		if (p.getGameMode() == GameMode.CREATIVE)
			return;

		if (cooldown.get(p) == true) {
			e.setCancelled(true);
			cooldown.put(p, false);
			p.setVelocity(p.getLocation().getDirection().multiply(1.6D).setY(1.0D));

			for (Player pl : Bukkit.getOnlinePlayers()) {
				pl.playEffect(p.getLocation(), Effect.MOBSPAWNER_FLAMES, 2004);
			}

			p.setAllowFlight(false);
		}
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onSneak(PlayerToggleSneakEvent e) {
		Player p = e.getPlayer();

		if (p.getGameMode() == GameMode.CREATIVE)
			return;

		if (p.isOnGround() == false && cooldown.get(p) != null && cooldown.get(p) == false) {
			p.setVelocity(new Vector(0, -7, 0));
			smash.add(p);
		}
	}

}