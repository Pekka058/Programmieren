package de.lobby.listener;

import java.util.ArrayList;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import de.lobby.main.Main;

public class BlöckeVerschwinden implements Listener{
	
	public BlöckeVerschwinden(Main main) {
	
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onBreak(BlockPlaceEvent e){
		final List<Location> locs = new ArrayList<>();
		final List<Material> materials = new ArrayList<>();
		final List<Byte> bytes = new ArrayList<>();
		
		Block b = e.getBlock();
		
		locs.add(b.getLocation());
		materials.add(b.getType());
		bytes.add(b.getData());
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(Main.plugin, new Runnable() {

			@Override
			public void run() {
				for(Location loc : locs){

						for(Byte by : bytes ){
							Block block = loc.getBlock();
							block.setData(by);
							block.setType(Material.REDSTONE_BLOCK);
							block.setType(Material.AIR);
							block.getWorld().playEffect(block.getLocation(), Effect.MOBSPAWNER_FLAMES, 3);
						
					}

				}
				
			}
			
		}, 2*20);
	}

}


