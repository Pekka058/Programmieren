package de.lobby.listener;

import java.io.File;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import de.lobby.main.Main;

public class MoveEvent implements Listener {
	
	File lobby = new File("plugins/LobbySystem", "config.yml");
	FileConfiguration lobbyfc = YamlConfiguration.loadConfiguration(lobby);
	
	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		Player player = e.getPlayer();
		Location Death = player.getLocation();
		
		if(Death.getBlockY() == 150) {
			if(!player.hasPermission("lobby.void")) {
			double x = lobbyfc.getDouble("Lobby.lobby.pos.x");
			double y = lobbyfc.getDouble("Lobby.lobby.pos.y");
			double z = lobbyfc.getDouble("Lobby.lobby.pos.z");
			float yaw = (float) lobbyfc.getDouble("Lobby.lobby.pos.yaw");
			float pitch = (float) lobbyfc.getDouble("Lobby.lobby.pos.pitch");
			World w = Bukkit.getWorld(lobbyfc.getString("Lobby.lobby.pos.world"));
			Location lc = new Location(w, x, y, z, yaw, pitch);
			player.teleport(lc);
			} else {
				player.sendMessage(Main.getInstance().TestPrefix + "�cHir ist die Void auf der H�he von �6" + Death.getBlockY());
				
				double x = lobbyfc.getDouble("Lobby.lobby.pos.x");
				double y = lobbyfc.getDouble("Lobby.lobby.pos.y");
				double z = lobbyfc.getDouble("Lobby.lobby.pos.z");
				float yaw = (float) lobbyfc.getDouble("Lobby.lobby.pos.yaw");
				float pitch = (float) lobbyfc.getDouble("Lobby.lobby.pos.pitch");
				World w = Bukkit.getWorld(lobbyfc.getString("Lobby.lobby.pos.world"));
				Location lc = new Location(w, x, y, z, yaw, pitch);
				player.teleport(lc);
			}
			
		}
		
	}

}
