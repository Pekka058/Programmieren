package de.lobby.listener;

import java.lang.reflect.Field;

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PlayerConnection;
import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerListHeaderFooter;

public class Tapdesign implements Listener {
	
	@EventHandler
	public void onjoin(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		Tap(e.getPlayer(), "§2§lWilkommen  §4✔ §7" + p.getName(), "\n§2§lauf §4§lFirekingdom.net");
	}

	public static void Tap(Player p, String header,String footer) {
		if(header == null) header = "";
		if(footer == null) footer = "";
		PlayerConnection connection = ((CraftPlayer)p).getHandle().playerConnection;
		
		IChatBaseComponent Title = ChatSerializer.a("{\"text\": \"" + header + "\"}");
		IChatBaseComponent Foot = ChatSerializer.a("{\"text\": \"" + footer + "\"}");
		
		PacketPlayOutPlayerListHeaderFooter headerPacket = new PacketPlayOutPlayerListHeaderFooter(Title);
		try {
			
			Field field = headerPacket.getClass().getDeclaredField("b");
			field.setAccessible(true);
			field.set(headerPacket, Foot);
			
		} catch (Exception e) {
			
			e.printStackTrace();
			 
		} finally {
			connection.sendPacket(headerPacket);
		}
	}

}
