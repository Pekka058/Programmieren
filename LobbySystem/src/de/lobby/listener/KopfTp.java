package de.lobby.listener;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import de.lobby.main.Main;

@SuppressWarnings("deprecation")
public class KopfTp implements Listener {

	public KopfTp(de.lobby.main.Main main) {
	}

	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if (e.getMaterial() == Material.SKULL_ITEM) {
				Inventory Kopf = Bukkit.createInventory(null, 54, "�6Players");

				for (Player players : Bukkit.getOnlinePlayers()) {
					if (players != e.getPlayer()) {
						ItemStack item = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
						ItemMeta meta = item.getItemMeta();
						meta.setDisplayName("�b�l�o" + players.getName());
						item.setItemMeta(meta);

						Kopf.addItem(item);

					}
				}
				e.getPlayer().openInventory(Kopf);
			}
		}
	}

	@EventHandler
	public void onKopfClick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();

		if (e.getInventory().getName().equalsIgnoreCase("�6Players")) {
			if (e.getSlot() == e.getRawSlot()) {
				if (e.getCurrentItem() != null && e.getCurrentItem().hasItemMeta()) {
					String playername = ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName());
					Player player_ = Bukkit.getPlayerExact(playername);

					if (player_ != null) {
						p.teleport(player_);
						p.sendMessage(Main.prefix + "�aTeleport zu �6" + player_.getName());
					}

				}

			}
		}
	}

}
