package de.lobby.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;

import de.lobby.commands.Build;

public class PickUp implements Listener {
	
	@EventHandler
	public void onPick(PlayerPickupItemEvent e) {
		Player p = e.getPlayer();
		if (Build.build.contains(p)) {
			e.setCancelled(false);
		} else if (!Build.build.contains(p)) {
			e.setCancelled(true);
		}
	}

}
