package de.lobby.itemBuilder;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemBuilder {

	private ItemStack item;
	private ItemMeta meta;

	public ItemBuilder(Material material) {
		item = new ItemStack(material);
		meta = item.getItemMeta();

	}

	public ItemBuilder(Material material, short subID) {
		item = new ItemStack(material, 1, subID);
		meta = item.getItemMeta();
	}

	public ItemBuilder setDisplayName(String name) {
		meta.setDisplayName(name);
		return this;
	}
	public ItemBuilder setLore(String...lore) {
		meta.setLore(Arrays.asList(lore));
		return this;
	}
	
	public ItemBuilder addEnchantment(Enchantment enchantment, int level) {
		meta.addEnchant(enchantment, level, true);
		return this;
	}
	
	public ItemBuilder setAmount(int amount) {
		item.setAmount(amount);
		return this;
	}
	
	public ItemStack build() {
		item.setItemMeta(meta);
		return item;
	}
	


}
