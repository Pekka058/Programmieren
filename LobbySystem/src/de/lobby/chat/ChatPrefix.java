package de.lobby.chat;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatPrefix implements Listener {
	
	@EventHandler
	public void onChat(AsyncPlayerChatEvent e) {
		Player player = e.getPlayer();
		String msg = e.getMessage();
		
		if(player.hasPermission("ls.chat.Owner")) {
			e.setFormat("�4�lOwner �8�l| �7" + player.getName() + " � " + msg);
			
		} else if(player.hasPermission("ls.chat.Admin")) {
			e.setFormat("�c�lAdmin �8�l| �7" + player.getName() + " � " + msg);
		} else if(player.hasPermission("ls.chat.Mod")) {
			e.setFormat("�c�lModderator �8�l | �7" + player.getName() + " � " + msg);
		} else {
			e.setFormat("�8�lSpieler | �7" + player.getName() + " � " + msg);
		}
	}

}
