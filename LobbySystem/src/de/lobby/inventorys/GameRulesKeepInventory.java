package de.lobby.inventorys;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;

public class GameRulesKeepInventory implements Listener {
	
	public final HashMap<String, ItemStack[]> inv = new HashMap<String, ItemStack[]>();
	
	@EventHandler
	public void onPlayerKeepInventory(GameRulesKeepInventory e) {
		e. onPlayerKeepInventory(e);
	}
	
	@EventHandler
    public void onDeath(PlayerDeathEvent e) {
      Player player = e.getEntity();
      List<ItemStack> newInventory = new ArrayList<ItemStack>();
           newInventory.add(player.getInventory().getHelmet());
           newInventory.add(player.getInventory().getChestplate());
           newInventory.add(player.getInventory().getLeggings());
           newInventory.add(player.getInventory().getBoots());
           newInventory.add(player.getItemInHand());

          ItemStack[] newStack =  newInventory.toArray(new ItemStack[newInventory.size()]);
          inv.put(player.getName(), newStack);
          e.getDrops().removeAll(newInventory);
        }

	

}
