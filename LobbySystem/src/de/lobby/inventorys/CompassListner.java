package de.lobby.inventorys;

import java.util.ArrayList;
import java.util.HashMap;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

public class CompassListner implements Listener {

	@EventHandler
	public void onCompClick(PlayerInteractEvent e) {
		if (e.getItem() == null)
			return;
		if (e.getAction() == Action.RIGHT_CLICK_BLOCK || e.getAction() == Action.RIGHT_CLICK_AIR) {
			if (e.getItem().getType() == Material.COMPASS) {
				Player p = e.getPlayer();
				openComp(p);
			}
		}
	}

	private HashMap<Player, Integer> continuem = new HashMap<>();

	private void openComp(final Player p) {
		final Inventory GUI = Bukkit.createInventory(p, 27, "�7Navigator");
		p.openInventory(GUI);
		continuem.put(p, 5);
		new BukkitRunnable() {

			@Override
			public void run() {
				int current = continuem.get(p);

				current--;

				continuem.put(p, current);
				setItemsFor(GUI, current);
				if (p.getOpenInventory() == null) {
					continuem.remove(p);
					cancel();
				}
				if (current == 1) {
					continuem.remove(p);
					cancel();
				}
			}
		}.runTaskTimer(de.lobby.main.Main.plugin, 0L, 3L);

	}

	private void setItemsFor(Inventory GUI, int c) {
		ItemStack glass = getStack(Material.STAINED_GLASS_PANE, 1, 15, " ", " ");
		GUI.clear();
		for (int a = 0; a < 27; a++) {
			if (c == 5) {
				for (int i = 0; i < 9; i++)
					GUI.setItem(i, glass);
				GUI.setItem(a, glass);

			}
			if (c == 4) {
				for (int i = 9; i < 18; i++)
					GUI.setItem(i, glass);
				GUI.setItem(a, glass);

			}
			if (c == 3) {
				for (int i = 18; i < 26; i++)
					GUI.setItem(i, glass);
				GUI.setItem(a, glass);
				GUI.setItem(9, getGunGame(Material.WOOD_AXE, 1, 0, " ", " "));
				GUI.setItem(5, getFFA(Material.IRON_SWORD, 1, 0, " ", " "));
			}
			if (c == 2) {
				for (int i = 26; i < 27; i++)
					GUI.setItem(i, glass);
				GUI.setItem(a, glass);
				GUI.setItem(11, getBedwars(Material.BED, 1, 0, " ", " "));
				GUI.setItem(9, getGunGame(Material.WOOD_AXE, 1, 0, " ", " "));
				GUI.setItem(5, getFFA(Material.IRON_SWORD, 1, 0, " ", " "));

			}
			if (c == 1) {
				GUI.setItem(a, glass);
				GUI.setItem(4, getSpawn(Material.NETHER_STAR, 1, 0, "�6Spawn", " "));
				GUI.setItem(11, getBedwars(Material.BED, 1, 0, " ", " "));
				GUI.setItem(9, getGunGame(Material.WOOD_AXE, 1, 0, " ", " "));
				GUI.setItem(5, getFFA(Material.IRON_SWORD, 1, 0, " ", " "));
			}
			if (c == 0) {
			}
		}

	}

	private ItemStack getStack(Material mat, int mege, int shvalue, String name, String lore) {
		ItemStack spawn = new ItemStack(mat, mege, (short) shvalue);
		ItemMeta lobby = spawn.getItemMeta();
		lobby.setDisplayName(" ");
		ArrayList<String> loreA = new ArrayList<>();
		loreA.add(lore);
		lobby.setLore(loreA);
		spawn.setItemMeta(lobby);

		return spawn;

	}

	private ItemStack getBedwars(Material mate, int menge, int shvaluee, String namee, String loore) {

		ItemStack Bedwars = new ItemStack(mate, menge, (short) shvaluee);
		ItemMeta bedwars = Bedwars.getItemMeta();
		bedwars.setDisplayName("�4Bed�fWars");
		ArrayList<String> looreA = new ArrayList<>();
		looreA.add(loore);
		bedwars.setLore(looreA);
		Bedwars.setItemMeta(bedwars);

		return Bedwars;

	}

	private ItemStack getGunGame(Material mate, int menge, int shvaluee, String namee, String loore) {

		ItemStack leer = new ItemStack(mate, menge, (short) shvaluee);
		ItemMeta leeer = leer.getItemMeta();
		leeer.setDisplayName("�6GunGame");
		ArrayList<String> looreA = new ArrayList<>();
		looreA.add(loore);
		leeer.setLore(looreA);
		leer.setItemMeta(leeer);

		return leer;

	}

	private ItemStack getFFA(Material mate, int menge, int shvaluee, String nammee, String looore) {

		ItemStack FFA = new ItemStack(mate, menge, (short) shvaluee);
		ItemMeta ffa = FFA.getItemMeta();
		ffa.setDisplayName("�6FFA");
		ArrayList<String> loooreA = new ArrayList<>();
		loooreA.add(looore);
		ffa.setLore(loooreA);
		FFA.setItemMeta(ffa);

		return FFA;

	}

	private ItemStack getSpawn(Material mate, int menge, int shvaluee, String namee, String loore) {

		ItemStack Spawn = new ItemStack(mate, menge, (short) shvaluee);
		ItemMeta spawn = Spawn.getItemMeta();
		spawn.setDisplayName("�6Spawn");
		ArrayList<String> looreA = new ArrayList<>();
		looreA.add(loore);
		spawn.setLore(looreA);
		Spawn.setItemMeta(spawn);

		return Spawn;

	}

}
