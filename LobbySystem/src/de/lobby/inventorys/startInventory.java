package de.lobby.inventorys;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import de.lobby.itemBuilder.ItemBuilder;

public class startInventory {

	public static void setStartinv(Player p) {
		p.getInventory().clear();
		
		ItemStack Navigator = new ItemBuilder(Material.COMPASS).setDisplayName("�6Navigator").build();
		ItemStack Hide = new ItemBuilder(Material.BLAZE_ROD).setDisplayName("�6Spieler verstecken").build();
		ItemStack schild = new ItemBuilder(Material.EYE_OF_ENDER).setDisplayName("�5VIP-Schild").build();
		ItemStack Extras = new ItemBuilder(Material.CHEST).setDisplayName("�6Extras").build();

		if (p.hasPermission("lobby.premium.schild")) {
			p.getInventory().setItem(2, schild);

		} else {
			p.getInventory().remove(schild);
		}

		p.getInventory().setItem(0, Navigator);
		p.getInventory().setItem(8, Hide);
		p.getInventory().setItem(4, Extras);
	}
}
