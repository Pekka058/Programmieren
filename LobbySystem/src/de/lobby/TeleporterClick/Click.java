package de.lobby.TeleporterClick;

import java.io.File;
import java.util.concurrent.CopyOnWriteArrayList;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import de.lobby.commands.Build;
import de.lobby.main.Main;

public class Click implements Listener {

	File lobby = new File("plugins/LobbySystem", "config.yml");
	FileConfiguration lobbyfc = YamlConfiguration.loadConfiguration(lobby);

	public static CopyOnWriteArrayList<Player> hidden = new CopyOnWriteArrayList<>();

	@EventHandler
	public void onInvetoryClick1(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		if (Build.build.contains(p)) {
			e.setCancelled(false);
		} else if (!Build.build.contains(p)) {
			e.setCancelled(true);
			if (e.getCurrentItem() != null) {
				if (e.getInventory().getName().equalsIgnoreCase("�7Navigator")) {
					if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�6Spawn")) {
						if (lobbyfc.getString("Lobby.lobby.pos.x") != null) {
							e.setCancelled(true);
							p.closeInventory();
							double x = lobbyfc.getDouble("Lobby.lobby.pos.x");
							double y = lobbyfc.getDouble("Lobby.lobby.pos.y");
							double z = lobbyfc.getDouble("Lobby.lobby.pos.z");
							float yaw = (float) lobbyfc.getDouble("Lobby.lobby.pos.yaw");
							float pitch = (float) lobbyfc.getDouble("Lobby.lobby.pos.pitch");
							World world = Bukkit.getWorld(lobbyfc.getString("Lobby.lobby.pos.world"));
							Location Spawn = new Location(world, x, y, z, yaw, pitch);
							p.sendMessage(Main.getInstance().TestPrefix + "�6Teleport to Lobby");
							p.teleport(Spawn);

						} else {
							if (p.hasPermission("ls.setspawn.see")) {
								p.closeInventory();
								p.sendMessage(Main.getInstance().TestPrefix
										+ "�cDer �6Spawn �cwurde noch nicht gesetzt!\n Nutze /ls setspawn");

							} else {
								p.closeInventory();
								p.sendMessage(Main.getInstance().TestPrefix
										+ "�cDer �6Spawn �cwurde noch nicht gesetzt!");
							}
						}

					}
					if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�6Knockit")) {
						if (lobbyfc.getString("Lobby.Knockit.pos.x") != null) {
							p.closeInventory();

							double x = lobbyfc.getDouble("Lobby.Knockit.pos.x");
							double y = lobbyfc.getDouble("Lobby.Knockit.pos.y");
							double z = lobbyfc.getDouble("Lobby.Knockit.pos.z");
							float yaw = (float) lobbyfc.getDouble("Lobby.Knockit.pos.yaw");
							float pitch = (float) lobbyfc.getDouble("Lobby.Knockit.pos.pitch");
							World world = Bukkit.getWorld(lobbyfc.getString("Lobby.Knockit.pos.world"));
							Location KnockIT = new Location(world, x, y, z, yaw, pitch);
							p.sendMessage(Main.getInstance().TestPrefix + "�6Teleport to KnockIT");
							p.teleport(KnockIT);
						} else {
							if (p.hasPermission("ls.setspawn.see")) {
								p.closeInventory();
								p.sendMessage(Main.getInstance().TestPrefix
										+ "�cDer Spawn f�r �5KnockIT �cwurde noch nicht gesetzt! Nutze /ls setgame KnockiT");

							} else {
								p.closeInventory();
								p.sendMessage(Main.getInstance().TestPrefix
										+ "�cDer Spiel modus �5KnockIT �cist noch nicht verf�gbar!");
							}
						}

					}
					if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�7FFA")) {
						if (lobbyfc.getString("Lobby.ffa.pos.x") != null) {
							p.closeInventory();

							double x = lobbyfc.getDouble("Lobby.ffa.pos.x");
							double y = lobbyfc.getDouble("Lobby.ffa.pos.y");
							double z = lobbyfc.getDouble("Lobby.ffa.pos.z");
							float yaw = (float) lobbyfc.getDouble("Lobby.ffa.pos.yaw");
							float pitch = (float) lobbyfc.getDouble("Lobby.ffa.pos.pitch");
							World world = Bukkit.getWorld(lobbyfc.getString("Lobby.ffa.pos.world"));
							Location FFA = new Location(world, x, y, z, yaw, pitch);
							p.sendMessage(Main.getInstance().TestPrefix + "�6Teleport to FFA");
							p.teleport(FFA);

						} else {
							if (p.hasPermission("ls.setspawn.see")) {
								p.closeInventory();
								p.sendMessage(Main.getInstance().TestPrefix
										+ "�cDer Spawn f�r �7FFA �cwurde noch nicht gesetzt! Nutze /ls setgame FFA!");
							} else {
								p.closeInventory();
								p.sendMessage(Main.getInstance().TestPrefix
										+ "�cDer Spiel modus �7FFA �cist noch nicht verf�gbar!");
							}
						}
					}

					if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�9Bed�bWars")) {
						if (lobbyfc.getString("Lobby.bedwars.pos.x") != null) {
							p.closeInventory();

							double x = lobbyfc.getDouble("Lobby.bedwars.pos.x");
							double y = lobbyfc.getDouble("Lobby.bedwars.pos.y");
							double z = lobbyfc.getDouble("Lobby.bedwars.pos.z");
							float yaw = (float) lobbyfc.getDouble("Lobby.bedwars.pos.yaw");
							float pitch = (float) lobbyfc.getDouble("Lobby.Knockit.pos.pitch");
							World world = Bukkit.getWorld(lobbyfc.getString("Lobby.bedwars.pos.world"));
							Location BedWars = new Location(world, x, y, z, yaw, pitch);
							p.sendMessage(Main.getInstance().TestPrefix + "�6Teleport to BedWars");
							p.teleport(BedWars);

						} else {
							p.closeInventory();
							if (p.hasPermission("ls.setgame.see")) {
								p.sendMessage(Main.getInstance().TestPrefix
										+ "�cDer Spawn f�r �9Bed�bWars �cwurde noch nicht gesetzt! Nutze /ls setgame BedWars!");
							} else {
								p.closeInventory();
								p.sendMessage(Main.getInstance().TestPrefix
										+ "�cDer Spiel modus �9Bed�bWars �cist noch nicht verf�gbar!");
							}
						}
					}
					if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�6Comunity")) {
						if (lobbyfc.getString("Lobby.Comunity.pos.x") != null) {
							p.closeInventory();

							double x = lobbyfc.getDouble("Lobby.Comunity.pos.x");
							double y = lobbyfc.getDouble("Lobby.Comunity.pos.y");
							double z = lobbyfc.getDouble("Lobby.Comunity.pos.z");
							float yaw = (float) lobbyfc.getDouble("Lobby.Comunity.pos.yaw");
							float pitch = (float) lobbyfc.getDouble("Lobby.Comunity.pos.pitch");
							World world = Bukkit.getWorld(lobbyfc.getString("Lobby.Comunity.pos.world"));
							Location Comunity = new Location(world, x, y, z, yaw, pitch);
							p.sendMessage(Main.getInstance().TestPrefix + "�6Teleport to Comunity");
							p.teleport(Comunity);

						} else {
							p.closeInventory();
							if (p.hasPermission("ls.setgame.see")) {
								p.sendMessage(Main.getInstance().TestPrefix
										+ "�cDer Spawn f�r �6Comunity �cwurde noch nicht gesetzt! Nutze /ls setgame Comunity!");
							} else {
								p.closeInventory();
								p.sendMessage(Main.getInstance().TestPrefix
										+ "�cDie Comunity ist leider nicht verf�gbar!");
							}
						}
					}
					if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�6MLG-Rush")) {
						if (lobbyfc.getString("Lobby.mlg.pos.x") != null) {
							p.closeInventory();

							double x = lobbyfc.getDouble("Lobby.mlg.pos.x");
							double y = lobbyfc.getDouble("Lobby.mlg.pos.y");
							double z = lobbyfc.getDouble("Lobby.mlg.pos.z");
							float yaw = (float) lobbyfc.getDouble("Lobby.mlg.pos.yaw");
							float pitch = (float) lobbyfc.getDouble("Lobby.mlg.pos.pitch");
							World world = Bukkit.getWorld(lobbyfc.getString("Lobby.mlg.pos.world"));
							Location Mlg = new Location(world, x, y, z, yaw, pitch);
							p.sendMessage(Main.getInstance().TestPrefix + "�6Teleport to MLG-Rush");
							p.teleport(Mlg);

						} else {
							p.closeInventory();
							if(p.hasPermission("ls.setgame.see")) {
								p.sendMessage(Main.getInstance().TestPrefix + "�cDer Spawn f�r �6MLG-Rush �cwurde noch nicht gesetzt! Nutze /ls setgame mlg");
							} else {
								p.closeInventory();
								p.sendMessage(Main.getInstance().TestPrefix + "�cDieser Spiel modus ist noch nicht verf�gbar!");
							}
							
						}
					}
					if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�5GunGame")) {
						if(lobbyfc.getString("Lobby.gungame.pos.x") != null) {
							p.closeInventory();
							
							double x = lobbyfc.getDouble("Lobby.gungame.pos.x");
							double y = lobbyfc.getDouble("Lobby.gungame.pos.y");
							double z = lobbyfc.getDouble("Lobby.gungame.pos.z");
							float yaw = (float) lobbyfc.getDouble("Lobby.gungame.pos.yaw");
							float pitch = (float) lobbyfc.getDouble("Lobby.gungame.pos.pitch");
							World world = Bukkit.getWorld(lobbyfc.getString("Lobby.gungame.pos.world"));
							Location GunGame = new Location(world, x, y, z, yaw, pitch);
							p.sendMessage(Main.getInstance().TestPrefix + "�6Teleport to GunGame");
							p.teleport(GunGame);
							
							
							
						} else {
							p.closeInventory();
							if(p.hasPermission("ls.setgame.see")) {
								p.sendMessage(Main.getInstance().TestPrefix + "�cDer Spawn f�r �6GunGame �cwurde noch nicht gesetzt! Nutze /ls setgame GunGame");
							} else {
								p.closeInventory();
								p.sendMessage(Main.getInstance().TestPrefix + "�cDieser Spiel modus ist noch nicht verf�gbar!");
							}
							
						}
					
					}
					if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�bSkyWars")) {
						if(lobbyfc.getString("Lobby.skywars.pos.x") != null) {
							p.closeInventory();
							
							double x = lobbyfc.getDouble("Lobby.skywars.pos.x");
							double y = lobbyfc.getDouble("Lobby.skywars.pos.y");
							double z = lobbyfc.getDouble("Lobby.skywars.pos.z");
							float yaw = (float) lobbyfc.getDouble("Lobby.skywars.pos.yaw");
							float pitch = (float) lobbyfc.getDouble("Lobby.skywars.pos.pitch");
							World world = Bukkit.getWorld(lobbyfc.getString("Lobby.skywars.pos.world"));
							Location SkyWars = new Location(world, x, y, z, yaw, pitch);
							p.sendMessage(Main.getInstance().TestPrefix + "�6Teleport to SkyWars");
							p.teleport(SkyWars);
							
							
						} else {
							p.closeInventory();
							if(p.hasPermission("ls.setgame.see")) {
								p.sendMessage(Main.getInstance().TestPrefix + "�cDer Spawn f�r �bSkyWars �cwurde noch nicht gesetzt! Nutze /ls setgame SkyWars");
								
							} else {
								p.closeInventory();
								p.sendMessage(Main.getInstance().TestPrefix + "�cDer Spiel modus ist leider nicht verf�gbar!");
							}
						}
					}
					if(e.getInventory().getName().equalsIgnoreCase("�6Extras")) {
						if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�6Boots")) {
							p.closeInventory();
						}
						
						
					}

				}
				if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�cKnockback-FFA")) {
					if(lobbyfc.getString("Lobby.Knockffa.pos.x") != null) {
						p.closeInventory();
						
						double x = lobbyfc.getDouble("Lobby.Knockffa.pos.x");
						double y = lobbyfc.getDouble("Lobby.Knockffa.pos.y");
						double z = lobbyfc.getDouble("Lobby.Knockffa.pos.z");
						float yaw = (float) lobbyfc.getDouble("Lobby.Knockffa.pos.yaw");
						float pitch = (float) lobbyfc.getDouble("Lobby.Knockffa.pos.pitch");
						World world = Bukkit.getWorld(lobbyfc.getString("Lobby.Knockffa.pos.world"));
						Location Knockffa = new Location(world, x, y, z,yaw, pitch);
						p.sendMessage(Main.getInstance().TestPrefix + "�6Teleport to Knock-FFA");
						p.teleport(Knockffa);
						
					} else {
						p.closeInventory();
						p.sendMessage(Main.getInstance().TestPrefix + "�cDer Spiel modus ist leider nicht verf�gbar!");
					}
				}

			}
			if(e.getInventory().getName().equalsIgnoreCase("�6Extras")) {
				if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�6Boots")) {
					p.closeInventory();
				}
				
				
			}

		}
	}

}
