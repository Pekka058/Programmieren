package de.lobby.main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import de.lobby.TeleporterClick.Click;
import de.lobby.chat.ChatPrefix;
import de.lobby.chat.Command_Block;
import de.lobby.commands.Build;
import de.lobby.commands.LobbySystem;
import de.lobby.commands.gm;
import de.lobby.commands.msg;
import de.lobby.commands.yt;
import de.lobby.extras.VIPschild;
import de.lobby.inventorys.GameRulesKeepInventory;
import de.lobby.inventorys.startInventory;
import de.lobby.listener.HidePlayer;
import de.lobby.listener.Jumpbads;
import de.lobby.listener.MoveEvent;
import de.lobby.listener.PickUp;
import de.lobby.listener.PlayerHiderListener;
import de.lobby.listener.Prefix;
import de.lobby.listener.Tapdesign;
import de.lobby.listener.Teleporter;
import de.lobby.listener.Werbungblock;
import de.lobby.listener.dooplejump;
import de.lobby.listener.sign;
import de.lobby.warps.setWarps;
import de.lobby.warps.warp_Command;

public class Main extends JavaPlugin implements Listener, CommandExecutor {

	public static String prefix = "�7[�eLobby�7] ", NO_PREMISSIONS = prefix + "�cKeine Rechte!";
	public String TestPrefix;
	public String Rechte;

	public static ArrayList<Player> inventory = new ArrayList<>();

	File lobby = new File("plugins/LobbySystem", "config.yml");
	FileConfiguration lobbyfc = YamlConfiguration.loadConfiguration(lobby);

	java.io.ByteArrayOutputStream b = new java.io.ByteArrayOutputStream();
	java.io.DataOutputStream out = new java.io.DataOutputStream(b);

	private static Main instance;
	public static Main plugin;
	public static Main KeineRechte;

	public void onLoad() {
		instance = this;
		KeineRechte = this;
	}

	public void onEnable() {
		Bukkit.getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
		plugin = this;
		Prefix.getConfigFile();
		Prefix.getConfigFileConfiguration();
		Prefix.setStartConfig();
		Prefix.readConfig();

		Prefix.setStartRechte();
		Prefix.getConfigKeineRechte();
		Prefix.getKeineRechte();
		Prefix.readKeineRechte();

		this.getCommand("ls").setExecutor(new LobbySystem());
		this.getCommand("gm").setExecutor(new gm());
		this.getCommand("Build").setExecutor(new Build());
		this.getCommand("setwarp").setExecutor(new setWarps());
		this.getCommand("Warp").setExecutor(new warp_Command());
		this.getCommand("yt").setExecutor(new yt());
		this.getCommand("msg").setExecutor(new msg());

		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(this, this);
		pm.registerEvents(new Jumpbads(), this);
		pm.registerEvents(new dooplejump(), this);
		pm.registerEvents(new Tapdesign(), this);
		pm.registerEvents(new GameRulesKeepInventory(), this);
		pm.registerEvents(new Teleporter(this), this);
		pm.registerEvents(new sign(this), this);
		pm.registerEvents(new Click(), this);
		pm.registerEvents(new Command_Block(), this);
		pm.registerEvents(new de.lobby.chat.Chatfilter(), this);
		pm.registerEvents(new PlayerHiderListener(), this);
		pm.registerEvents(new VIPschild(this), this);
		// pm.registerEvents(new ExtrasInventory(), this);
		pm.registerEvents(new Werbungblock(), this);
		pm.registerEvents(new ChatPrefix(), this);
		pm.registerEvents(new MoveEvent(), this);
		pm.registerEvents(new PickUp(), this);

		onregister();
		getInstance();

		System.out.println("[LobbySystem] LobbySystem wurde erfolgreich activiert");
		Bukkit.getServer().getConsoleSender()
				.sendMessage(Main.getInstance().TestPrefix + "�aDas LobbySystem wurde gestartet");

		this.getLogger().log(Level.INFO, "[Broadcaster] Plugin activated");

		List<String> originalList = lobbyfc.getStringList("broadcast");
		List<String> newList = new ArrayList<>();

		for (String s : originalList) {
			newList.add(s.replace("&", "�"));
		}

	}

	public void onregister() {

		getConfig().options().copyDefaults(true);
		saveConfig();
		if (!lobby.exists()) {
			try {
				lobby.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}

		} else {
			try {
				lobbyfc.load(lobby);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InvalidConfigurationException e) {
				e.printStackTrace();
			}
		}
	}

	private HashMap<Player, Integer> Jump = new HashMap<>();

	@SuppressWarnings("unused")
	private void openComp(final Player p) {
		Jump.put(p, 5);
		new BukkitRunnable() {

			@Override
			public void run() {
				int current = Jump.get(p);

				current--;

				Jump.put(p, current);
				if (p.getOpenInventory() == null) {
					Jump.remove(p);
					cancel();
				}
				if (current == 1) {
					Jump.remove(p);
					cancel();
				}
			}
		}.runTaskTimer(de.lobby.main.Main.plugin, 0L, 3L);

	}

	// BlockBreakEvent
	@SuppressWarnings("unlikely-arg-type")
	@EventHandler
	public void onInteract(BlockBreakEvent e) {
		Player p = e.getPlayer();
		if (Build.build.contains(p)) {
			e.setCancelled(false);
		} else if (!Build.build.contains(p)) {
			e.setCancelled(true);
			if (e.getPlayer().getWorld().equals(getConfig().getString("Lobby.world.w"))) {
				if (e.getPlayer().getWorld().equals("world")) {
					e.setCancelled(true);
					Build.build.add(p);
				}
			}
		}
	}

	// /hub Command
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player p = (Player) sender;
		if (p.hasPermission("command.setlobby")) {
			if (p instanceof Player) {
				if (cmd.getName().equalsIgnoreCase("lobby")) {
					double x = getConfig().getDouble("Lobby.lobby.pos.x");
					double y = getConfig().getDouble("Lobby.lobby.pos.y");
					double z = getConfig().getDouble("Lobby.lobby.pos.z");
					float yaw = (float) getConfig().getDouble("Lobby.lobby.pos.yaw");
					float pitch = (float) getConfig().getDouble("Lobby.lobby.pos.pitch");
					World w = Bukkit.getWorld(getConfig().getString("Lobby.lobby.pos.world"));
					Location lobby = new Location(w, x, y, z, yaw, pitch);
					startInventory.setStartinv(p);
					p.teleport(lobby);
				}

			}

		}

		return true;
	}

	@EventHandler
	public void onPlayerChange(PlayerChangedWorldEvent e) {
		Player p = e.getPlayer();
		if (p.getWorld().getName().equalsIgnoreCase("LobbyStandartNeuRe")) {

		}

	}

	// Player Quit Event
	@EventHandler
	public void onleave(PlayerQuitEvent e) {
		Player p = e.getPlayer();
		if (!p.hasPermission("ls.prefix.quit")) {
			String quitmessage = getConfig().getString("Lobby.messages.quit");
			quitmessage = ChatColor.translateAlternateColorCodes('&', quitmessage);
			quitmessage = quitmessage.replace("[Player]", p.getDisplayName());
			e.setQuitMessage(quitmessage);
		} else {
			e.setQuitMessage(Main.getInstance().TestPrefix + "�7�lDas �9�lTeammitglied �6�l" + p.getDisplayName()
					+ " �7�lhat den Server verlassen!");
		}
	}

	// Player Join Event
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		if (!p.hasPermission("ls.prefix.join")) {
			startInventory.setStartinv(p);
			String joinmessage = getConfig().getString("Lobby.messages.join");
			joinmessage = ChatColor.translateAlternateColorCodes('&', joinmessage);
			joinmessage = joinmessage.replace("[Player]", e.getPlayer().getDisplayName());
			e.setJoinMessage(joinmessage);
			double x = getConfig().getDouble("Lobby.lobby.pos.x");
			double y = getConfig().getDouble("Lobby.lobby.pos.y");
			double z = getConfig().getDouble("Lobby.lobby.pos.z");
			float yaw = (float) getConfig().getDouble("Lobby.lobby.pos.yaw");
			float pitch = (float) getConfig().getDouble("Lobby.lobby.pos.pitch");
			World world = Bukkit.getWorld("world");
			Location lobbby = new Location(world, x, y, z, yaw, pitch);
			p.teleport(lobbby);
			Build.invSave.get(p);
			for (Player hiders : HidePlayer.hidden) {
				hiders.hidePlayer(e.getPlayer());
			}

		} else {
			startInventory.setStartinv(p);
			e.setJoinMessage(Main.getInstance().TestPrefix + "�7�lDas �9�lTeammitglied �6�l" + p.getDisplayName()
					+ " �7�lhat den Server betreten!");
			double x = getConfig().getDouble("Lobby.lobby.pos.x");
			double y = getConfig().getDouble("Lobby.lobby.pos.y");
			double z = getConfig().getDouble("Lobby.lobby.pos.z");
			float yaw = (float) getConfig().getDouble("Lobby.lobby.pos.yaw");
			float pitch = (float) getConfig().getDouble("Lobby.lobby.pos.pitch");
			World world = Bukkit.getWorld("world");
			Location lobbby = new Location(world, x, y, z, yaw, pitch);
			p.teleport(lobbby);

		}

	}

	public static Main getInstance() {
		return Main.instance;
	}

	public static Plugin getCore() {

		return null;
	}

	public static Main getKeineRechte() {
		return KeineRechte;
	}

}
